﻿using System;
using System.IO;
using System.Collections.Generic;
using Microsoft.Win32;
using System.IO;
using System.Runtime.InteropServices;
using System.Reflection;

namespace cambia_sfondo_3._0
{
    /// <summary>
    /// rickychii, 2015.01.27
    /// revision 4.0, 2024.10.30
    /// </summary>
    /// 

    class Program
    {
        internal static String path = "";

        internal static String currentWallpaper = "";
        internal static String newWallpaper = "";

        [DllImport("user32.dll", CharSet = CharSet.Auto)]
        private static extern Int32 SystemParametersInfo(UInt32 action, UInt32 uParam, String vParam, UInt32 winIni);

        internal static readonly UInt32 SPI_SETDESKWALLPAPER = 0x14;
        internal static readonly UInt32 SPIF_UPDATEINIFILE = 0x01;
        internal static readonly UInt32 SPIF_SENDWININICHANGE = 0x02;

        static void Main(string[] args)
        {
            try
            {

                //ALGORITHM IN PAUSE?
                if (File.Exists(Environment.CurrentDirectory + "/" + "PAUSEWALLPAPER")) {
                    return;
                }

                StreamReader sr = new StreamReader(Environment.CurrentDirectory + "/" + "wallpapers.cfg");
                path = sr.ReadLine();
                sr.Close();

                // Lista per accumulare i risultati
                List<string> allFilesARRAY = new List<string>();

                // Array delle estensioni desiderate
                string[] extensions = { "*.jpg", "*.jpeg", "*.png", "*.webp" };

                // Per ogni estensione, ottieni i file e aggiungili alla lista
                foreach (string ext in extensions) {
                    string[] files = Directory.GetFiles(path, ext, SearchOption.AllDirectories);
                    allFilesARRAY.AddRange(files);
                }

                RegistryKey RegistryWallpaper = Registry.CurrentUser.OpenSubKey(@"Control Panel\Desktop", true);

                if (allFilesARRAY.Count < 2)
                    throw new Exception("Directory is empty or directory has only one file.");

                RandomizeYourWallpaper:

                Random r = new Random();
                int RandomIndex = r.Next(allFilesARRAY.Count);
                newWallpaper = allFilesARRAY[RandomIndex];

                if(currentWallpaper.Equals(newWallpaper, StringComparison.OrdinalIgnoreCase))
                    goto RandomizeYourWallpaper;

                RegistryWallpaper.SetValue("Wallpaper", newWallpaper);
                RegistryWallpaper.Close();

                //SCRIVO SU FILE DI LOG:
                // Ottieni la directory principale dell'utente
                string userDirectory = Environment.GetFolderPath(Environment.SpecialFolder.DesktopDirectory);
                String myfile = userDirectory + "\\wallpapers.log";

                // Aggiunge la stringa al file (creandolo se non esiste)
                System.IO.File.AppendAllText(myfile, "[" + DateTime.Now.ToString("yyyy-MM-dd HH.mm:ss") + "]: " + newWallpaper + Environment.NewLine);

                ReloadSettings();
            }
            catch (Exception ex) {
                Console.WriteLine(ex.Message);
            }
        }
        internal static void ReloadSettings()
        {
            SystemParametersInfo(SPI_SETDESKWALLPAPER, 0, newWallpaper, SPIF_UPDATEINIFILE | SPIF_SENDWININICHANGE);
        }
    }
}